
require "test/unit"
require "./cart"

=begin
This class has two attributes: 1 - Array with all rules; 2 - Array with all purchased products.
With this attributes and methods is possible to do CheckOuts.
=end

class CheckOut
  attr_accessor :rules, :cart

  def initialize(rules)
    @rules = rules
    @cart = []
  end


  #This method is responsible to do scanning each products is purchased and add to cart
  #Each product is counted and added if it's not in cart

  def scan(item)
    status = true  #This attribute is responsible to control if product exists or not in cart
    if(@cart.empty?)
      new_item = Cart.new(item,1)
      @cart.push(new_item)
    else

      #Verify if new product is in cart, if it's in cart is counted its amount, otherwise it's added in cart
      for i in 0..(@cart.length-1)
        if(item == @cart[i].product)
          @cart[i].amount += 1
          status = true
          return
        else
          status = false
        end
      end
      if(status == false)
        new_item = Cart.new(item,1)
        @cart.push(new_item)
      end
    end
  end


  #This method is responsible to do total sum of purchased products in cart to according its unit price or special offer.

  def total()
    purchase_total = 0
    for i in 0..(@cart.length-1)
      for j in 0..(@rules.length-1)
        if(@cart[i].product == @rules[j].product)

      #This condition is to verify if exist offer special to product
      #If attributte 'offer_amount' is different of 0, it is true( It is explained in Rule Class)
          if(@rules[j].offer_amount != 0)

            #Special offer is only applied if product amount in cart is more or equal offer amount(It makes sense)
            if(@cart[i].amount >= @rules[j].offer_amount)

              #set_number is a number of groups about fixed quantity of offer special
              set_number = (@cart[i].amount / @rules[j].offer_amount)

              #units is a number of products that will be calculated with its unit prices(It is out Special Offer)
              units = (@cart[i].amount).remainder @rules[j].offer_amount
              purchase_total += (set_number * @rules[j].offer_prince) + (units * @rules[j].unit_price)

            #If amount in cart is less than special offer, the products are calculated with its unit prices)
            else
              purchase_total += @cart[i].amount * @rules[j].unit_price
            end
          else
            purchase_total += @cart[i].amount * @rules[j].unit_price
          end
        end
      end
    end
    #puts "\r\n Purchase Total:"
    #puts purchase_total
    purchase_total
  end





end
