=begin

This class is to do a Rule Object and to facilitate all operations about it
All atributes are:
:product -> It is a product name(Ex.: 'A')
:unit_price -> It is a unit price of the corresponding product(Ex.: 50)
:offer_amount -> It is offer amount about special offer (Ex.: 3)
This attribute above is to identify if determited product has special offer,
Because if is 0 it means that has not special offer (So product is calculated only with unit_price)
:offer_prince -> If exist special offer, then offer_amount is different of 0, and so should exist a price
And offer_prince is a price about special offer(Ex.: 130), if not exist special offer special its value is 0

Resume: Rule.new('A',50,3,130) -> product with special offer
        Rule.new('C',20,0,0) - > product without special offer
=end

class Rule
  attr_accessor :product, :unit_price, :offer_amount, :offer_prince


  def initialize(product, unit_price, offer_amount, offer_prince)
    @product = product
    @unit_price = unit_price
    @offer_amount = offer_amount
    @offer_prince = offer_prince
  end

end
