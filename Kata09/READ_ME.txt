#####################################################################################################################
                      Nathanael Araujo da Silva

*************************** About Scripts ***************************************************************************

In this Kata09 Project we have 4 files:

/test_cases.rb
In this file we have all the tests that will be run, such as your initial setup.

/checkout.rb
In this file we have all the functions that will be used to manipulate
the products bought, such as the applied price rules and calculation
of the sum of the purchase based on the cart and pricing strategies.

/rule.rb
In this file we have the definition of the Class Rule that will be used
for the creation of price rules.

/cart.rb
In this file we have the definition of the Class Cart that will be used
to create the cart with the products purchased.


*************************** Objectives of the Kata ******************************************************************


1. It is necessary that the format of the rules can be well done so that the
checkout can be carried out effectively according to how pricing strategies applied.
If we observe that each rule can be an object being created and applied at the end of the purchase,
and in the same way that the shopping cart can contain several objects of products to be purchased,
you can make a match between each product that this cart with its matched object of rule of price for a checkout.
At the end, we can propose a format that contains the product in question,
together with your unit price and then your price strategy,
and so we have a complete product with your prices to do any checkout.

2.When each price rule is treated as an object we are able to manipulate the data structure better,
because any changes that happen to add new rules will be applied to that object, besides that we can create methods
within that object for the effective manipulation of the object of according to the need of the project in the future.
And since the cart also has a class to be instantiated as an object, it is possible to modify the match strategy
between the purchased products and their rules.
In this way we also make flexible products that do not have any price rules, it is not necessary to modify the
structure or have another data structure to manipulate these two types(With or without price rules)
