Feature: New Group

As a iOS Whatsapp user
I want to do all operations in new group
So that i can use better this feature

Scenario: Creating a new group
    Given that i am in the "Chats" screen
    When i click on "New Group" on the top right
    And i click on search text area and input text "John" as username from my contact list
    And i click on "John" username that i searched
    And i click on "Next" on the top right
    And i input text "Mygroup" as new group name
    And i click on "Create" on the top right
    Then i should see a message 'You created group "MyGroup"'

Scenario: Adding a new participants in the create group being a group admin
    Given that i am in the "Chats" screen
    When i click on the group name "MyGroup"
    And i click on the top screen on the group name "MyGroup"
    And i click on "Add Participants" button
    And i click on search text area and input text "Mary" as username from my contact list
    And i click on "Mary" username that i searched
    And i click on "Add" on the top right
    And i click on "Add" on confirm pop up
    Then i should see "Mary" name in the list of group participants and it's added in alphabetic order

Scenario: Deleting a participants in the created group
    Given that i am in the "Chats" screen
    When i click on the group name "MyGroup"
    And i click on the top screen on the group name "MyGroup"
    And i click on "John" name in the list of group participants
    And i click on "Remove John" option
    And i click on "Remove" confirm option
    Then i should not see "John" in list of group participants

Scenario: Mute a created group
    Given that i am in the "Chats" screen
    When i click on the group name "MyGroup"
    And i click on the top screen on the group name "MyGroup"
    And i click on "Mute" option
    And i click on "1 year" option
    Then i should see "Mute" option with label "until" + "Date of next year" + "hour" (Format Date:DD/MM/YY)

Scenario: Verify if label text about number of participants is to according with real number of participants
    Given that i am in the "Chats" screen and my group "Mygroup" has "5" participants
    When i click on the group name "MyGroup"
    And i click on the top screen on the group name "MyGroup"
    Then label text about number of participants should be to according with real number of participants

Scenario: Changing group name
    Given that i am in the "Chats" screen
    When i click on the group name "MyGroup"
    And i click on the top screen on the group name "MyGroup"
    And i click on group name "MyGroup"
    And i delete group name
    And i input new group name "MyNewGroup"
    And i click on "Save" on the top right
    Then i should see the new group name "MyNewGroup" in Group info

Scenario: Checking is not possible make call in create group
    Given that i am in the "Chats" screen
    When i click on the group name "MyGroup"
    Then phone icon should NOT appear on top right beside group name

Scenario: Allowed group name size
    Given that i am in the "Chats" screen
    When i click on the group name "MyGroup"
    And i click on the top screen on the group name "MyGroup"
    And i click on group name "MyGroup"
    And i delete group name
    And i input text with '16' chars
    Then i am not allowed adding more chars

Scenario: Exit group
    Given that i am in the "Chats" screen
    When i click on the group name "MyGroup"
    And i click on the top screen on the group name "MyGroup"
    And i click on "Exit Group" option
    And i lick on "Exit Group" confirm popup
    Then i should see a message "You're no longer a participant in this group"

Scenario: Deleting group
    Given that i am in the "Chats" screen and i've exited group
    When i swipe for left group name "MyGroup"
    And i click on "More" option
    And i click on "Delete Group" option
    And i click on "Delete Group" confirm popup
    Then i should NOT see a group name in my "Chats" list
