Feature: Setting some particular configurations

As a iOS Whatsapp user
I want to do some particular operations in my configurations
So that i can use custom this feature of my way

Scenario: Blocking a contact from my contact list
    Given that i am in "Settings" screen
    When i click on "Account" option
    And i click on "Privacy" option
    And i click on "Blocked" option
    And i click on "Add New..." option
    And i click on search text area and input text "Mary" as username from my contact list
    And i click on "Mary" username that i searched
    Then i should see "Mary" name on the list of blocked contacts and it's added in alphabetic order

Scenario: Don't Share Last Seen
    Given that i am in "Settings" screen
    When i click on "Account" option
    And i click on "Privacy" option
    And i click on "Last Seen" option
    And i click on "Nobody" option
    Then "Nobody" option should be marked

Scenario: Changing profile status
    Given i am on "Settings" screen
    When i click on my profile name "Richard"
    And i click on below "STATUS" text (on text box)
    And i click on below "YOUR CURRENT STATUS IS:" text (on text box)
    And i delete current status
    And i input new text profile status "This is my Status"
    And i click on "Save" on the Top right
    Then new status "This is my new Status" should be seen in current status

Scenario: Changing notifications sound
    Given that i am in "Settings" screen
    When i click on "Notifications" option
    And i click on "Sound" option
    And i choose "Aurora" as notification sound
    And i click on "Save" option on the top right
    Then i should see text "Aurora" in text box  beside Sound option
